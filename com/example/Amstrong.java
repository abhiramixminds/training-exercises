/**
 * This  is a program to find amstrong number or not
 *
 * @author Abhirami
 */
package com.example;

import java.util.Scanner;
import java.lang.Math;

public class Amstrong {

    public static void main(String[] args) {
        int x, y, z = 0, temp;
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter A Number");
        x = myObj.nextInt();
        temp = x;
        while (x > 0) {
            y = x % 10;
            x = x / 10;
            z = (int) (z + Math.pow(y, 3));
        }
        if (temp == z) {
            System.out.println(temp + " is an Armstrong Number.");
        } else {
            System.out.println(temp + " is not an Armstrong Number.");
        }
        myObj.close();
    }
}
