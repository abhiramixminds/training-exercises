/**
 * Find duplicate words in a sentence “ I am am learning java programming”.
 *
 * @author Abhirami
 */
package com.example;
import java.util.*;
public class Dupilcate {
    public static void main(String[] args)
    {
        String expression = "I am am learning java  programming";

        // splitting words using regex
        String[] words = expression.split("\\W");

        // creating object of HashSet class implemented by
        Set<String> set = new HashSet<>();

        // here we are iterating in Array
        for (int i = 0; i < words.length - 1; i++) {

            for (int j = 1; j < words.length; j++) {
                if (words[i].equals(words[j]) && i != j) {
                    set.add(words[i]);
                }
            }
        }

        System.out.println(set);
    }
}
