/**
 *This  is a program to find the highest mark scored among 5 subjects by a student.  Use for each to traverse the array.
 * @author Abhirami
 */
package com.example;
import java.util.Scanner;
import java.io.*;

public class Student {
    public static void main(String[] args) {
        Scanner scannerObj  = new Scanner(System.in);
        int a[] = new int[5];
        System.out.println("Enter the mark of 5 subjects:");
        for(int i = 0; i < 5; i++)
        {
            a[i] = scannerObj.nextInt();
        }
        StudentNew obj=new StudentNew(a);
        System.out.println("Highest mark is "+obj.findHighest());
    }
}

class StudentNew {
   int subject[];
    StudentNew(int subject[]) {
        this.subject = subject;
    }
         int findHighest()
        {
            int high=subject[0];
            for(int number:subject)
             {
               if(number>high)
                {
                  high=number;
                }
             }
           return high;
         }

}