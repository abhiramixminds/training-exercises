/**
 * This  is a Program to find the average of marks in five subjects and the grade for the student.
 *
 * @author Abhirami
 */
package com.example;

import java.util.Scanner;

public class StudentDetails {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        int a[] = new int[5];
        System.out.println("Enter the mark of 5 subjects:");

        for (int i = 0; i < 5; i++) {
            a[i] = scannerObj.nextInt();
        }
        StudentDetailsNew obj = new StudentDetailsNew(a);
        System.out.println(obj.findGrade());
    }

}

class StudentDetailsNew {
    int subject[];
    Double total = 0.00, average, percentage;
    String grade = "";

    StudentDetailsNew(int subject[]) {
        this.subject = subject;
    }

    String findGrade() {

        for (int number : subject) {
            total += number;
        }
        average = total / 5.0;
        percentage = (total / 500.0) * 100;
        if (average >= 0 && average <= 50) {
            grade = "E";

        } else if (average > 50 && average <= 60) {
            grade = "D";
        } else if (average > 60 && average <= 70) {
            grade = "C";
        } else if (average > 70 && average <= 80) {
            grade = "B";
        } else if (average > 80 && average <= 90) {
            grade = "A";
        } else if (average > 90) {
            grade = "A+";
        }
        String result = "average : " + average + " \n Grade :" + grade;

        return result;
    }

}

