package com.example;
/**
 * A class BankAccount with instance variables  accNumber,‘balance’ and ‘rate’ of interest in the class. Create a method to calculate the interest and update the balance say  currentBalance() inside an inner class and make it private, Write main method to show the customer’s currentBalance.
 *
 * @author Abhirami
 */

import java.util.Scanner;

class BankAccount {

    private double bal;
    private String accno, accname;

    BankAccount(String accno, String accname, Double bal) {
        this.accno = accno;
        this.accname = accname;
        this.bal = bal;
    }

    private class Interest {
        static Double rate = 4.5;

        void findInterest() {
            double interest = bal * rate / 100;
            bal += interest;
            System.out.println("Accno:" + accno);
            System.out.println("Accno:" + accname);
            System.out.println("Current Balance :" + bal);
        }
    }

    // Accessing he inner class from the method within
    void displayBalance() {
        Interest in = new Interest();
        in.findInterest();
    }
}

class Myclass {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        String accname, accno;
        double bal;
        System.out.println("Enter Account Holder Name:");
        accname = scannerObj.nextLine();
        System.out.println("Enter Account No:");
        accno = scannerObj.nextLine();
        System.out.println("Enter Balance:");
        bal = scannerObj.nextDouble();
        BankAccount bankaccountobj = new BankAccount(accno, accname, bal);
        bankaccountobj.displayBalance();
    }
}
