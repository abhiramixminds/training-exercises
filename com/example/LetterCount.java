/**
 * This  is a program to letter space number and other charcter from a sentence
 *
 * @author Abhirami
 */
package com.example;

import java.util.Scanner;
import java.util.*;

public class LetterCount {
    public static String countLetterNumberSpaceString(String stringText) {
        char[] ch = stringText.toCharArray();
        int letterCount = 0, spaceCount = 0, numberCount = 0, otherCharacterCount = 0;

        for (int i = 0; i < stringText.length(); i++) {
            if (Character.isLetter(ch[i])) {
                letterCount++;
            } else if (Character.isDigit(ch[i])) {
                numberCount++;
            } else if (Character.isSpaceChar(ch[i])) {
                spaceCount++;
            } else {
                otherCharacterCount++;
            }
        }
        String result = "The string  " + stringText + " has\n" + letterCount + " letters\n" +
                spaceCount + " space:\n" + numberCount + "numbers\n" +
                "other: " + otherCharacterCount;
        return result;

    }

    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        String stringText = "", resultText;
        System.out.println("Enter Your Text");
        stringText = scannerObj.nextLine();
        System.out.println(countLetterNumberSpaceString(stringText));
    }
}

