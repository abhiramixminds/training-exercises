package com.example;

/**
 * Write a program to find area of shapes by implementing method overloading.
 *
 * @author Abhirami
 */
public class AreaofShapewithMethodOverriding {
    public static void main(String args[]) {
        Shapes ob = new Shapes();
        ob.area(5);
        ob.area(11, 12);
        ob.area(2.5);
    }

}

class Shapes {
    void area(float x) {
        System.out.println("Area of the square is " + Math.pow(x, 2) + " sq units");
    }

    void area(float x, float y) {
        System.out.println("Area of the rectangle is " + x * y + " sq units");
    }

    void area(double x) {
        double z = (22 * x * x) / 7;
        System.out.println("Area of the circle is " + z + " sq units");
    }
}