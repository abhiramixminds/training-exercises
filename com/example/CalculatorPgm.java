/**
 * This  is a program to generate a simple calculator.
 *
 * @author Abhirami
 */
package com.example;

import java.util.Scanner;

public class CalculatorPgm {
    public static void main(String[] args) {
        char operator;
        Double number1, number2, result;
        Scanner scannerObj = new Scanner(System.in);
        System.out.println("Choose an operator: +, -, *, /");
        operator = scannerObj.next().charAt(0);
        System.out.println("Enter the first number");
        number1 = scannerObj.nextDouble();
        System.out.println("Enter the second number");
        number2 = scannerObj.nextDouble();
        switch (operator) {
            case '+':
                result = number1 + number2;
                System.out.println("Result: " + result);
                break;
            case '-':
                result = number1 - number2;
                System.out.println("Result: " + result);
                break;
            case '*':
                result = number1 * number2;
                System.out.println("Result: " + result);
                break;
            case '/':
                result = number1 / number2;
                if (number2 == 0) {
                    System.out.println("Please enter a valid number");
                }
                System.out.println("Result: " + result);
                break;

        }
        scannerObj.close();
    }
}

