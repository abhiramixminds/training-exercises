/**
 * This  is a Program to convert decimal number to binary.
 *
 * @author Abhirami
 */
package com.example;

import java.util.Scanner;

public class DecimalToBinary {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        int number;
        System.out.println("Enter Decimal Number");
        number = scannerObj.nextInt();
        int binary[] = new int[40];
        int index = 0;
        while (number > 0) {
            binary[index++] = number % 2;
            number = number / 2;
        }
        System.out.println("Decimal of " + number + " is: ");
        for (int i = index - 1; i >= 0; i--) {
            System.out.print(binary[i]);
        }
        System.out.println();
        scannerObj.close();
    }
}

