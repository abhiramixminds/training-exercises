package com.example;
/**
 * Write a Class Employee and create object without using new operator.”.
 *
 * @author Abhirami
 */

public class ObjectwithNewInstanceMethod {

    public static void main(String[] args) {
        Employee obj = null;
        try {
            obj = (Employee) new ObjectwithNewInstanceMethod().getClass()
                    .getClassLoader().loadClass("com.example.Employee").newInstance();// Fully qualified classname should be used.

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        obj.setEmpid(10);
        obj.setEmpname("Abhirami");
        System.out.println(obj);


    }
}

class Employee {
    int empid;
    String empname;



    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empid=" + empid +
                ", empname='" + empname + '\'' +
                '}';
    }
}

