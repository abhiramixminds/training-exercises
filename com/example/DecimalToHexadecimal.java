/**
 * This  is a Program to convert decimal number to hexadecimal number.
 *
 * @author Abhirami
 */
package com.example;

import java.util.Scanner;

public class DecimalToHexadecimal {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        int decimalNumber, rem, decimalNumberEntered;
        String hexDecimal = "";
        char hexchars[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        System.out.println("Enter Decimal Number");
        decimalNumber = scannerObj.nextInt();
        decimalNumberEntered = decimalNumber;
        while (decimalNumber > 0) {
            rem = decimalNumber % 16;
            hexDecimal = hexchars[rem] + hexDecimal;
            decimalNumber = decimalNumber / 16;
        }
        System.out.println("Hexadecimal of " + decimalNumberEntered + " is: " + hexDecimal);
    }
}
