/**
 * Write a program to find area of shapes by implementing method overriding.
 *
 * @author Abhirami
 */
package com.example;

import java.util.*;

abstract class Shape {
    private int dimension1, dimension2;

    abstract public void areaCalculation();

    public void readData() {
        Scanner scannerObj = new Scanner(System.in);
        System.out.println("Enter two dimensions:");
        dimension1 = scannerObj.nextInt();
        dimension2 = scannerObj.nextInt();
    }

    public int getDimension1() {
        return dimension1;
    }

    public int getDimension2() {
        return dimension2;
    }

}

class Rectangle extends Shape {
    private int area;

    public void areaCalculation() {
        int x = super.getDimension1();
        int y = super.getDimension2();
        area = x * y;
    }

    public void displayData() {
        System.out.println("Area of Rectangle:" + area);
    }
}

class Triangle extends Shape {
    private int area;

    public void areaCalculation() {
        int x = super.getDimension1();
        int y = super.getDimension2();
        area = (1 / 2) * x * y;
    }

    public void displayData() {
        System.out.println("Area of Triangle:" + area);
    }

}

class Square extends Shape {
    private int area;

    public void areaCalculation() {
        int x = super.getDimension1();
        area = x * x;
    }

    public void displayData() {
        System.out.println("Area of Square:" + area);
    }

}

public class ShapesAreaCalculation {
    public static void main(String args[]) {
        Scanner scannerObj = new Scanner(System.in);
        Shape d1;
        Rectangle r1 = new Rectangle();
        r1.readData();
        r1.areaCalculation();
        r1.displayData();
        Triangle t1 = new Triangle();
        t1.readData();
        t1.areaCalculation();
        t1.displayData();
        Square s1 = new Square();
        s1.readData();
        s1.areaCalculation();
        s1.displayData();
    }
}
