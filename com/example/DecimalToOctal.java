/**
 * This  is a program to decimal to octal conversion
 *
 * @author Abhirami
 */
package com.example;

import java.util.Scanner;

public class DecimalToOctal {
    public static String toOctal(int decimalNumber) {
        int remainder;
        String octalNumber = "";
        char octalchars[] = {'0', '1', '2', '3', '4', '5', '6', '7'};
        while (decimalNumber > 0) {
            remainder = decimalNumber % 8;
            octalNumber = octalchars[remainder] + octalNumber;
            decimalNumber = decimalNumber / 8;
        }
        return octalNumber;
    }

    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        int decimalNumber;
        System.out.println("Enter Decimal Number");
        decimalNumber = scannerObj.nextInt();
        System.out.println("Decimal to octal of " + decimalNumber + " is: " + toOctal(decimalNumber));
    }
}
