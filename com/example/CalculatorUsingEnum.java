/**
 * Write a simple calculator using enum”.
 *
 * @author Abhirami
 */
package com.example;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
enum MathOperator { Add, Subtract, Multiply, Divide, };
public class CalculatorUsingEnum {
    MathOperator op;
    Double number1,number2;
    public CalculatorUsingEnum(Double number1,Double number2,MathOperator op){
        this.op = op;
        this.number1 = number1;
        this.number2 = number2;
    }



    public void Operation(Double number1, Double number2, MathOperator operator)
    {
        double result;

        switch (operator)
        {
            case Add:
                result = number1 + number2;
                System.out.println("Result: " + result);
                break;
            case Subtract:
                result = number1 - number2;
                System.out.println("Result: " + result);
                break;
            case Multiply:
                result = number1 * number2;
                System.out.println("Result: " + result);
                break;
            case Divide:
                result = number1 / number2;
                System.out.println("Result: " + result);
                break;
            default:
                System.out.println("Pls try agian" );
                break;
        }

    }

    public static void main(String args[])throws IOException
    {
        Double number1, number2;
        String operator;
        BufferedReader scannerObj = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the first number");
        number1 = Double.parseDouble(scannerObj.readLine());
        System.out.println("Enter the second number");
        number2 = Double.parseDouble(scannerObj.readLine());
        System.out.println("Choose an operator: Add,Subtract,Multiply,Divide");
        operator = scannerObj.readLine();
        MathOperator op=MathOperator.valueOf(operator);
        CalculatorUsingEnum e1 = new CalculatorUsingEnum(number1,number2,op);
        e1.Operation(number1,number2,op);


    }
}
