/**
 * This  is a Program to perform string functions.
 *
 * @author Abhirami
 */
package com.example;
import java.util.Scanner;

public class StringHandling {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        char strFunction;
        String stringText1="",stringText2="";
        System.out.println("Enter the first string:");
        stringText1 = scannerObj.nextLine();
        System.out.println("Enter the second string:");
        stringText2 = scannerObj.nextLine();
        System.out.println("Choose a string function:\n 1=>String Length \n 2=>String Compare \n 3=>String Concat \n 4=>String IsEmpty \n 5=>String Trim \n 6=>String toUpper \n 7=>String toLowerCase \n 8=>String replace \n 9=>String endsWith \n 0=>split");
        strFunction = scannerObj.next().charAt(0);
        switch (strFunction) {
            case '1':
                System.out.println( "length of : "+stringText1+ "="+ stringText1.length());
                break;
            case '2':
                System.out.println("Compare Result of " + stringText1 + " and " + stringText2 + "=" + stringText1.compareTo(stringText2));
                break;
            case '3':
                System.out.println("Concat Result of " + stringText1 + " and " + stringText2 + "=" + stringText1.concat(stringText2));
                break;
            case '4':
                System.out.println("Isempty Result of " + stringText1 + "=" + stringText1.isEmpty());
                break;
            case '5':
                System.out.println("Trim Result of " + stringText1 + " and " + stringText2 + "=" + stringText1.trim());
                break;
            case '6':
                System.out.println("Uppercase Result of " + stringText1 + "=" + stringText1.toUpperCase());
                break;
            case '7':
                System.out.println("Lowercase Result of " + stringText1 + "=" + stringText1.toLowerCase());
                break;
            case '8':
                System.out.println("Replace Result of " + stringText1 + "=" + stringText1.replace('h','t'));
                break;
            case '9':
                System.out.println("Endswith Result of " + stringText1 + "=" + stringText1.endsWith("u"));
                break;
            case '0':
                System.out.println("Split " + stringText1 + "with "+stringText2+" =\n" );
                String[] words=stringText1.split(stringText2);
                for(String w:words){
                    System.out.println(w);
                }
                break;
        }
        scannerObj.close();
    }
}
