/**
 * Write a program to perform various string operations with built in methods of StringBuilder  class.
 *
 * @author Abhirami
 */
package com.example;
import java.util.Scanner;
import java.io.*;
import java.util.concurrent.LinkedBlockingQueue;
public class StringBuilderTest {
    public static void main(String[] argv)
            throws Exception
    {
        Scanner scannerObj = new Scanner(System.in);
        char strFunction;
        String stringText1="",stringText2="";
        System.out.println("Enter the first string:");
        stringText1 = scannerObj.nextLine();
        System.out.println("Enter the second string:");
        stringText2 = scannerObj.nextLine();
        System.out.println("Choose a string function: 1=>length  2=>Capacity 3=>Append 4=>insert 5=>reverse 6=>Delete 7=>replace 8=>toString() 9=>substring");
        strFunction = scannerObj.next().charAt(0);
        StringBuilder str = new StringBuilder(stringText1);
        switch (strFunction) {
            case '1':
                System.out.println("Length of string "+stringText1 +" = " + str.length());
                break;
            case '2':
                System.out.println("Capacity of string "+stringText1 +" = " + str.capacity());
                break;
            case '3':
                System.out.println("Result of append "+stringText1 +" with "+stringText2 +" = " + str.append(stringText2));
                break;
            case '4':
                System.out.println("Result of Insert "+stringText2+" At position 5 of "+stringText1 +" = " + str.insert(5,stringText2));
                break;
            case '5':
                System.out.println("Reverse of "+stringText1+" = " + str.reverse());
                break;
            case '6':
                System.out.println("Deleting first 5  character of "+stringText1+" = " + str.delete(0, 5));
                break;
            case '7':
                System.out.println("Replacing 5 to 8  character of "+stringText1+" with are = " + str.replace(5, 8, "are"));
                break;
            case '8':
                System.out.println("toString of "+stringText1+" = " + str.toString());
                break;
            case '9':
                System.out.println("SubSequence of "+stringText1+" = " + str.substring(5));
                break;
        }
        scannerObj.close();

    }
}
