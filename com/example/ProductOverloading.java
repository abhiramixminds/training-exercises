/**
 * Write a program to find product of numbers by implementing method overloading.
 *
 * @author Abhirami
 */
package com.example;

public class ProductOverloading {
    public static void main(String args[]) {
        Product ob = new Product();
        ob.findProduct(5, 6);
        ob.findProduct(5, 6, 7);
        ob.findProduct(5.7, 6.2);
        ob.findProduct(5.777, 6.278);

    }


}

class Product {

    void findProduct(int firstNumber, int secondNumber) {
        System.out.println("product of two integers:" + firstNumber * secondNumber);
    }

    void findProduct(int firstNumber, int secondNumber, int thirdNumber) {
        System.out.println("product of three integer numbers:" + firstNumber * secondNumber * thirdNumber);
    }

    void findProduct(float firstNumber, float secondNumber) {
        System.out.println("product of two floating numbers:" + firstNumber * secondNumber);
    }

    void findProduct(double firstNumber, double secondNumber) {
        System.out.println("product of two double numbers:" + firstNumber * secondNumber);
    }


}
